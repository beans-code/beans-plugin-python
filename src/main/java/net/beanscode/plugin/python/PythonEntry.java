package net.beanscode.plugin.python;

import java.io.IOException;

import net.beanscode.model.BeansSettings;
import net.beanscode.model.cass.notification.Notification;
import net.beanscode.model.cass.notification.NotificationType;
import net.beanscode.model.notebook.NotebookEntry;
import net.beanscode.model.notebook.NotebookEntryStatus;
import net.beanscode.model.notebook.ReloadPropagator;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.json.JsonUtils;

public class PythonEntry extends NotebookEntry {
	
	private static final String META_CODE 			= "code";
	private static final String META_COMPILEDCODE 	= "compiledcode";

	public PythonEntry() {
		
	}

	@Override
	public String getShortDescription() {
		return "Python entry";
	}

	@Override
	public String getSummary() {
		return "Python script";
	}

	@Override
	public boolean isReloadNeeded() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ReloadPropagator reload() throws ValidationException, IOException {
		reload(true);
		return ReloadPropagator.BLOCK;
	}

	@Override
	public String getEditorClass() {
		return "net.beanscode.web.view.plugins.python.PythonEntryEditorPanel";
	}

	@Override
	public void start() throws ValidationException, IOException {
		FileExt log = getPythonLogFile();
		if (log != null && log.exists())
			log.delete();
		log = getPythonOutFile();
		if (log != null && log.exists())
			log.delete();
		
		new Notification()
			.setId(getId())
			.setDescription(getName())
			.setNotificationType(NotificationType.INFO)
			.setTitle("Python code started...")
			.setUserId(getUserId())
			.save();
		
		reload();
	}

	@Override
	public boolean isRunning() throws ValidationException, IOException {
		return getStatus() != null
				&& getStatus() == NotebookEntryStatus.RUNNING;
	}

	@Override
	public void stop() throws ValidationException, IOException {
		// TODO Auto-generated method stub
		
	}
	
	public void reload(boolean inBackground) throws ValidationException, IOException {
		if (inBackground) {
			new PythonEntryJob(this).save();
			return;
		}
		
		try {
			// clear previous
			getMeta().remove(META_COMPILEDCODE);
			for (String key : getMeta().getKeys()) {
				if (key.equals(META_CODE) == false)
					getMeta().remove(key);
			}
			
			// status: started
			setStatus(NotebookEntryStatus.RUNNING);
			save();
			
			new Notification()
				.setId(getId())
				.setDescription(getName())
				.setNotificationType(NotificationType.INFO)
				.setTitle("Python started...")
				.setUserId(getUserId())
				.save();
			
			// clear tmp data structures
//			getId2datafile().clear();
	
			// converting some potential BEANS queries or columns names in gnuplot script to pure gnuplot
			FileExt workingDir = new FileExt("$HOME/.beans-software/plugins/python/", getId().getId());
			workingDir.mkdirs();
			setPythonWrapper(new PythonWrapper(compileGnuplotScript(workingDir))
								.setPythonFilename("python-wrapper-" + getId() + ".py")
								.setWorkingFolderPath(workingDir.getAbsolutePath()));
			
			// prepare input data for gnuplot (if needed)
//			prepareData();
	
			LibsLogger.info(PythonEntry.class, "Original python code: \n" + getPythonCode());
			LibsLogger.info(PythonEntry.class, "BEANS compiled python code: \n" + getPythonCompiledCode());
			
			// run actuall gnuplot
			getPythonWrapper().run();
			
			int i = 0;
			for (FileExt output : getPythonWrapper().iterateOutputFiles()) {
				setMeta("file-name-" + (i++), output.getAbsolutePath());
			}
			
			// status: finished
			setStatus(NotebookEntryStatus.SUCCESS);
			save();
			
			new Notification()
				.setId(getId())
				.setDescription(getName())
//				.setLocked(true)
				.setNotificationType(NotificationType.OK)
				.setTitle("Python successfull")
				.setUserId(getUserId())
				.save();
		} catch (Throwable t) {
			LibsLogger.error(PythonEntry.class, "Python failed", t);
			
			new Notification()
				.setId(getId())
				.setDescription(getName())
				.setNotificationType(NotificationType.ERROR)
				.setTitle("Python failed")
				.setUserId(getUserId())
				.save();
		}
	}
	
	public FileExt getPythonLogFile() {
		return new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(), 
				"/plugins/python/",
				"/python-" + getId() + ".log");
	}
	
	public FileExt getPythonOutFile() {
		return new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(), 
				"/plugins/python/" + getId() + "/python.out");
	}
	
	private String compileGnuplotScript(FileExt workingFolder) throws IOException {
		StringBuilder sb = new StringBuilder();
		
		// read last session
		UUID prevEntry = getNotebook().getPreviousEntry(getId());
		if (prevEntry != null) {
			FileExt workingDir = new FileExt("$HOME/.beans-software/plugins/python/", prevEntry.getId());
			FileExt loadSession = new FileExt(workingDir.getAbsolutePath(), "dill-" + prevEntry + ".db");
			if (loadSession.exists()) {
				sb.append("import dill\n");
				sb.append("dill.load_session('" + loadSession.getAbsolutePath() + "')\n");
			}
		}
		
		// add the python code
		sb.append(getPythonCode());
		sb.append("\n");
		
		// save session
		FileExt dumpSession = new FileExt(workingFolder, "dill-" + getId() + ".db");
		sb.append("import dill\n");
		sb.append("dill.dump_session('" + dumpSession.getAbsolutePath() + "')\n");
		
		setPythonCompiledCode(sb.toString());
		
		return getPythonCompiledCode();
	}

	public String getPythonCode() {
		return getMetaAsString(META_CODE, "");
	}
	
	public PythonEntry setPythonWrapper(PythonWrapper pyWrapper) {
		setMeta("pythonWrapper", JsonUtils.objectToString(pyWrapper));
		return this;
	}
	
	public PythonWrapper getPythonWrapper() {
		String tmp = getMetaAsString("pythonWrapper", null);
		return tmp != null ? JsonUtils.fromJson(tmp, PythonWrapper.class) : null;
	}
	
	public PythonEntry setPythonCode(String code) {
		setMeta(META_CODE, code);
		return this;
	}
	
	public String getPythonCompiledCode() {
		return getMetaAsString(META_COMPILEDCODE, "");
	}
	
	public PythonEntry setPythonCompiledCode(String code) {
		setMeta(META_COMPILEDCODE, code);
		return this;
	}
}
