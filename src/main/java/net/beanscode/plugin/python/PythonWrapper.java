package net.beanscode.plugin.python;

import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.file.FileUtils;
import net.hypki.libs5.utils.file.LazyFileIterator;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.system.Basher;

import com.google.gson.annotations.Expose;

public class PythonWrapper {
	
	@Expose
	private String pythonScript = null;
	
	@Expose
	private String workingFolder = null;
	
	@Expose
	private String pythonFilename = null;

	public PythonWrapper() {
		
	}
	
	public PythonWrapper(String pythonScript) {
		setPythonScript(pythonScript);
	}

	public String getPythonScript() {
		return pythonScript;
	}

	public PythonWrapper setPythonScript(String pythonScript) {
		this.pythonScript = pythonScript;
		return this;
	}
	
	public PythonWrapper run() throws IOException {
		try {
			final FileExt pyFile = getPythonFile();
			
			// create folder 
			getWorkingFolder().mkdirs();

			// save python file
			FileUtils.saveToFile(getPythonScript(), pyFile, true);
			
			// run python
			Basher.exec(new String[]{"python", pyFile.getAbsolutePath()},
					getWorkingFolder(),
					new FileWriter(new FileExt(getWorkingFolder(), "python.out").getAbsolutePath()));
			
			return this;
		} catch (Throwable t) {
			throw new IOException("Python wrapper failed", t);
		}
	}
	
	public PythonWrapper clone() {
		return JsonUtils.fromJson(JsonUtils.objectToString(this), PythonWrapper.class);
	}
	
	public void delete() throws IOException {
		getWorkingFolder().remove(true);
	}
	
	public void clear() throws IOException {
		getWorkingFolder().cleanDir();
	}
	
	public Iterable<FileExt> iterateOutputFiles() throws IOException {
		final FileExt f = getWorkingFolder();
		
		return new Iterable<FileExt>() {
			@Override
			public Iterator<FileExt> iterator() {
				return new LazyFileIterator(f, true, true, false, new FileFilter() {
						@Override	
						public boolean accept(File pathname) {
							String tmp = pathname.getName();
							if (tmp.equals(getPythonFilename())
									|| tmp.startsWith("input-data-")
									|| tmp.equalsIgnoreCase("python.out")
									|| tmp.matches("dill-[\\d\\w]+.db"))
								return false;
							return true;
						}
					});
			}
		};
	}
	
	public FileExt getWorkingFolder() throws IOException {
		return new FileExt(getWorkingFolderPath());
	}

	public String getWorkingFolderPath() throws IOException {
		if (workingFolder == null) {
			workingFolder = FileUtils.createTmpRandomDir().getAbsolutePath();			
		}
		return workingFolder;
	}

	public PythonWrapper setWorkingFolderPath(String workingFolder) {
		this.workingFolder = workingFolder;
		return this;
	}
	
	public FileExt getPythonFile() throws IOException {
		return new FileExt(getWorkingFolderPath(), getPythonFilename());
	}

	public String getPythonFilename() {
		if (this.pythonFilename == null) {
			this.pythonFilename = "python-wrapper-" + UUID.random().getId() +  ".py";
		}
		return pythonFilename;
	}

	public PythonWrapper setPythonFilename(String pythonFilename) {
		this.pythonFilename = pythonFilename;
		return this;
	}
}
