package net.beanscode.plugin.python;

import java.io.IOException;

import com.google.gson.annotations.Expose;

import net.beanscode.model.BeansSettings;
import net.beanscode.model.notebook.NotebookEntryFactory;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.weblibs.jobs.Job;
import net.sf.oval.constraint.AssertValid;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class PythonEntryJob extends Job {
	
	@Expose
	@NotNull
	@NotEmpty
	@AssertValid
	private UUID pythonEntryId = null;
	
	private PythonEntry pythonEntry = null;

	public PythonEntryJob() {
		
	}
	
	public PythonEntryJob(PythonEntry ge) {
		setPythonEntryId(ge.getId());
	}
	
	@Override
	protected String getLogSeparately() {
		return new FileExt(BeansSettings.getBeansHomeFolder().getAbsolutePath(), 
				"/plugins/python/",
				"/python-" + getPythonEntryId() + ".log")
			.getAbsolutePath();
	}
	
	@Override
	public void run() throws IOException, ValidationException {
		LibsLogger.info(PythonEntryJob.class, "Starting gnuplot entry wrapper");
		
		getPythonEntry().reload(false);
		
//		for (FileExt f : getGnuplotEntry().getGnuplotWrapper().iterateOutputFiles())
//			LibsLogger.info(GnuplotEntryJob.class, "Output: " + f.getAbsolutePath());
		
		LibsLogger.info(PythonEntryJob.class, "Finished gnuplot entry wrapper");
	}
	
	public PythonEntry getPythonEntry() throws IOException {
		if (pythonEntry == null)
			pythonEntry = (PythonEntry) NotebookEntryFactory.getNotebookEntry(getPythonEntryId());
		return pythonEntry;
	}

	public UUID getPythonEntryId() {
		return pythonEntryId;
	}

	public void setPythonEntryId(UUID pythonEntryId) {
		this.pythonEntryId = pythonEntryId;
	}
}
